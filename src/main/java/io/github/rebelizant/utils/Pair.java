package io.github.rebelizant.utils;

/**
 * Simple tuple type
 *
 * @author rebelizant
 *         Created on 12.12.15.
 *
 * @param <K> type of the key
 * @param <V> type of the value
 */
public class Pair<K, V> {

    private K key;

    private V value;

    private Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    /**
     * Returns new pair from the specified key and value
     *
     * @param key tuple's key
     * @param value tuple's value
     * @param <K> type of the key
     * @param <V> type of the value
     * @return new tuple with key and value
     */
    public static <K, V> Pair<K, V> of(K key, V value) {
        return new Pair<>(key, value);
    }

    /**
     * Returns key of the tuple
     *
     * @return key
     */
    public K key() {
        return key;
    }

    /**
     * Returns value of the tuple
     *
     * @return value
     */
    public V value() {
        return value;
    }

}
