package io.github.rebelizant.utils;

import java.util.Arrays;
import java.util.List;

/**
 * @author rebelizant
 *         Created on 07.02.16.
 */
public class KeyUtils {

    @SafeVarargs
    public final <T> List<T> key(T... values) {
        return Arrays.asList(values);
    }

}
