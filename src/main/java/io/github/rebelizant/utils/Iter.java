package io.github.rebelizant.utils;

import java.util.function.Supplier;

/**
 * Utility class to iterate chunk of code {@code n} times or while {@code Supplier<Boolean> condition}
 *
 * @author rebelizant
 *         Created on 05.12.15.
 */
public abstract class Iter {

    /**
     * Returns {@code Iter} instance that will execute code {@code n} times
     *
     * @param times iterations' count
     * @return iter object
     * @throws IllegalArgumentException if times less than zero
     */
    public static TimesIter times(int times) {
        if (times < 0) {
            throw new IllegalArgumentException("Times cannot be less then 0. times=" + times);
        }
        return new TimesIter(times);
    }

    /**
     * Returns {@code Iter} instance that will execute code {@code n} times
     * <br>
     * Creates iter with 0 times if the specified count is less than zero
     *
     * @param times iterations' count
     * @return iter object
     */
    public static TimesIter timesSafe(int times) {
        return new TimesIter(times < 0 ? 0 : times);
    }

    /**
     * Returns {@code Iter} instance that will execute code while condition is {@code true}
     *
     * @param condition supplier of the {@code boolean} value
     * @return iter object
     */
    public static WhileIter whileTrue(Supplier<Boolean> condition) {
        return new WhileIter(condition);
    }

    /**
     * Executes specified chunk of code
     *
     * @param runnable code to execute
     */
    public abstract void execute(Runnable runnable);

    public static class TimesIter extends Iter {

        int times;

        public TimesIter(int times) {
            this.times = times;
        }

        @Override
        public void execute(Runnable runnable) {
            for (int i = 0; i < times; i++) {
                runnable.run();
            }
        }
    }

    public static class WhileIter extends Iter {
        Supplier<Boolean> condition;

        public WhileIter(Supplier<Boolean> condition) {
            this.condition = condition;
        }

        @Override
        public void execute(Runnable runnable) {
            while (condition.get()) {
                runnable.run();
            }
        }
    }


}
