package io.github.rebelizant.utils;

import java.util.concurrent.Callable;

/**
 * Utils for tests
 *
 * @author rebelizant
 *         Created on 06.12.15.
 */
public class TestUtils {

    /**
     * Throw cause of the exception that is thrown by the callable
     *
     * @param callable target code to execute
     * @param <V> return type of the callable
     * @return result of the callable code
     * @throws Throwable the cause of the exception that is thrown by the callable
     */
    public static  <V> V throwCause(Callable<V> callable) throws Throwable {
        try {
            return callable.call();
        } catch (Exception e) {
            throw e.getCause();
        }
    }

}
