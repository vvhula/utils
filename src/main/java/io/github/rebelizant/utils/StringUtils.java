package io.github.rebelizant.utils;

import java.util.Arrays;
import java.util.List;

/**
 * @author rebelizant
 *         Created on 08.01.16.
 */
public class StringUtils {

    public static boolean isEmpty(String value) {
        return value == null || value.length() == 0;
    }

    public static boolean isNotEmpty(String value) {
        return !isEmpty(value);
    }

    public static boolean noEmpty(String... values) {
        return NullUtils.isNotNull(values) && noEmpty(true, Arrays.asList(values));
    }

    private static boolean noEmpty(boolean result, List<String> values) {
        if (NullUtils.isNull(values) || values.size() < 1) return result;
        return noEmpty(result && isNotEmpty(values.get(0)), values.subList(1, values.size()));
    }

    public static boolean allEmpty(String... values) {
        return NullUtils.isNull(values) || allEmpty(true, Arrays.asList(values));
    }

    public static void throwIllegalArgumentExceptionIfEmpty(String value, String message, String... args) {
        if (isEmpty(value)) {
            throw new IllegalArgumentException(String.format(message, args));
        }
    }

    private static boolean allEmpty(boolean result, List<String> values) {
        if (NullUtils.isNull(values) || values.size() < 1) return result;
        return allEmpty(result && isEmpty(values.get(0)), values.subList(1, values.size()));
    }

}
