package io.github.rebelizant.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Contains utility methods for Java Reflection API
 *
 * @author rebelizant
 *         Created on 05.12.15.
 */
public final class ReflectionUtils {

    /**
     * Checks whether field is annotated with the specified annotation or not.
     *
     * @param element annotated element to check
     * @param annotation annotation to check
     * @param <T> type of the target
     * @param <A> type of the annotation
     * @return {@code true} if
     */
    public static <T extends AnnotatedElement,A extends Annotation> boolean isAnnotatedWith(T element, Class<A>
            annotation) {
        return NullUtils.isNotNull(element.getAnnotation(annotation));
    }

    /**
     * Returns {@code true} if the specified {@link AnnotatedElement} is annotated with some annotation
     * that annotated with the specified {@link Annotation}.
     *
     * @param element target element
     * @param annotation annotation to check
     * @param <T> type of the element
     * @param <A> type of the annotation
     * @return {@code true} if the element is annotated with some annotation that is annotated with the specified one
     * , {@code false - otherwise}
     */
    public static <T extends AnnotatedElement, A extends Annotation> boolean hasAnnotationAnnotatedWith(T element, Class<A> annotation) {
        for (Annotation a : element.getAnnotations()) {
            if (NullUtils.isNotNull(a.annotationType().getAnnotation(annotation))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns fields of the specified object that are annotated with the specified {@link Annotation}
     *
     * @param target target object
     * @param annotation target annotation
     * @param <T> type of the target
     * @param <A> type of the annotation
     * @return fields that are annotated with {@code annotation}
     */
    public static <T, A extends Annotation> List<Field> fieldsAnnotatedWith(T target, Class<A> annotation) {
        List<Field> result = Stream.of(target.getClass().getDeclaredFields())
                .filter(declaredField -> isAnnotatedWith(declaredField, annotation))
                .collect(Collectors.toList());
        result.addAll(Stream.of(target.getClass().getFields())
                        .filter(field -> isAnnotatedWith(field, annotation))
                        .collect(Collectors.toList()));
        return result;
    }

    /**
     * Returns fields of the specified object that are annotated with some annotation that is annotated with
     * the specified annotation
     *
     * @param target target object
     * @param annotation target annotation
     * @param <T> type of the object
     * @param <A> type of the annotation
     * @return fields that has annotation which is annotated with {@code annotation}
     */
    public static <T, A extends Annotation> List<Field> fieldsMarkedWith(T target, Class<A> annotation) {
        List<Field> result = Stream.of(target.getClass().getDeclaredFields())
                .filter(declaredField -> hasAnnotationAnnotatedWith(declaredField, annotation))
                .collect(Collectors.toList());
        result.addAll(Stream.of(target.getClass().getFields())
                .filter(field -> hasAnnotationAnnotatedWith(field, annotation))
                .collect(Collectors.toList()));
        return result;
    }

    /**
     * Fills field of the specified object with the specified value
     *
     * @param target target object
     * @param fieldName the name of the field
     * @param value value to set
     * @param <T> type of the object
     * @param <V> type of the value
     */
    public static <T, V> void setDeclaredField(T target, String fieldName, V value) {
        setDeclaredField(target, getDeclaredField(target, fieldName), value);
    }

    /**
     * Fills field of the specified object with the specified value
     *
     * @param target target object
     * @param field target field
     * @param value value to set
     * @param <T> type of the object
     * @param <V> type of the value
     */
    public static <T, V> void setDeclaredField(T target, Field field, V value) {
        makeAccessible(field);
        setDeclaredField2(target, field, value);
    }

    /**
     * Returns value of the declared field of the specified object
     *
     * @param target target object
     * @param fieldName the name of the field
     * @param <T> type of the object
     * @param <V> type of the value of the declared field
     * @return value of the declared field
     */
    public static <T, V> V getDeclaredFieldValue(T target, String fieldName) {
        return getDeclaredFieldValue(target, getDeclaredField(target, fieldName));
    }

    /**
     * Returns value of the declared field of the specified object
     *
     * @param target target object
     * @param field target field
     * @param <T> type of the object
     * @param <V> type of the value
     * @return value of the declared field
     */
    public static <T, V> V getDeclaredFieldValue(T target, Field field) {
        try {
            makeAccessible(field);
            @SuppressWarnings("unchecked")
            V o = (V) field.get(target);
            return o;
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns {@code true}, if the value of the specified field is {@code null}, {@code false} - otherwise
     *
     * @param target target object
     * @param fieldName the name of the field
     * @param <T> type of the object
     * @return {@code true} if the value of the field is {@code null}, {@code false} - otherwise
     */
    public static <T> boolean valueNull(T target, String fieldName) {
        return NullUtils.isNull(getDeclaredFieldValue(target, fieldName));
    }

    /**
     * Returns {@code true}, if the value of the specified field is {@code null}, {@code false} - otherwise
     *
     * @param target target object
     * @param field target field
     * @param <T> type of the object
     * @return {@code true} if the value of the field is {@code null}, {@code false} - otherwise
     */
    public static <T> boolean valueNull(T target, Field field) {
        return NullUtils.isNull(getDeclaredFieldValue(target, field));
    }

    /**
     * Returns {@code true}, if the value of the specified field is NOT {@code null}, {@code false} - otherwise
     *
     * @param target target object
     * @param fieldName the name of the field
     * @param <T> type of the object
     * @return {@code true} if the value of the field is NOT {@code null}, {@code false} - otherwise
     */
    public static <T> boolean valueNotNull(T target, String fieldName) {
        return !valueNull(target, fieldName);
    }

    /**
     * Returns {@code true}, if the value of the specified field is NOT {@code null}, {@code false} - otherwise
     *
     * @param target target object
     * @param field target field
     * @param <T> type of the object
     * @return {@code true} if the value of the field is NOT {@code null}, {@code false} - otherwise
     */
    public static <T> boolean valueNotNull(T target, Field field) {
        return !valueNull(target, field);
    }

    /**
     * Returns {@code true} if there is no field that contains {@code null} value, {@code false} - otherwise
     *
     * @param target target object
     * @param fields target fields
     * @param <T> type of the object
     * @return {@code true} if no field contains {@code null} value, {@code false} - otherwise
     */
    public static <T> boolean noNullValues(T target, Field... fields) {
        boolean result = true;
        for (Field field : fields) {
            result &= valueNotNull(target, field);
        }
        return result;
    }

    private static <T, V> void setDeclaredField2(T target, Field field, V value) {
        try {
            field.set(target, value);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private static void makeNonFinal(Field field) {
        try {
            Field modifiers = Field.class.getDeclaredField("modifiers");
            modifiers.setAccessible(true);
            modifiers.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private static void makeAccessible(Field field) {
        field.setAccessible(true);
    }

    /**
     * Returns declared field of the specified object
     *
     * @param target target object
     * @param fieldName the name of the field
     * @param <T> type of the object
     * @return declared field of the target object
     * @throws RuntimeException if target object contains no field with the specified name
     */
    public static <T> Field getDeclaredField(T target, String fieldName) {
        try {
            return target.getClass().getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns declared field of the specified class
     *
     * @param clazz target class
     * @param fieldName the name of the field
     * @return declared field of the target class
     * @throws RuntimeException if target class contains no field with the specified name
     */
    public static Field getDeclaredField(Class<?> clazz, String fieldName) {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns new instance of the specified class using default constructor.
     * <br>Specified class must contain public constructor with no arguments
     *
     * @param clazz class to instantiate
     * @param <T> type of the class
     * @return new instance of the class
     */
    public static <T> T instance(Class<T> clazz) {
        try {
            return clazz.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns new instance of the specified class using constructor with specified
     * parameters' types
     *
     * @param clazz class to instantiate
     * @param params parameters of the constructor
     * @param <T> type of the class
     * @return new instance of the specified class
     */
    public static <T> T instance(Class<T> clazz, Object... params) {
        try {
            List<Class<?>> paramTypes = Stream.of(params)
                    .map(Object::getClass)
                    .collect(Collectors.toList());
            return clazz.getConstructor(paramTypes.toArray(new Class<?>[paramTypes.size()])).newInstance(params);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns values of the declared fields of the target object
     *
     * @param target target object
     * @param <T> type of the target
     * @param <V> common type of the declared fields' values
     * @return array of the declared fields' values
     */
    public static <T, V> V[] getDeclaredFieldValues(T target, String... ignoredFields) {
        List<String> ignoredFieldsList = Arrays.asList(ignoredFields);
        List<V> result = new ArrayList<>();
        for (Field field : target.getClass().getDeclaredFields()) {
            if (!ignoredFieldsList.contains(field.getName())) {
                result.add(getDeclaredFieldValue(target, field));
            }
        }
        @SuppressWarnings("unchecked")
        V[] vs = (V[]) result.toArray();
        return vs;
    }

}
