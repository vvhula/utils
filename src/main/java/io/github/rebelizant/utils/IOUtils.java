package io.github.rebelizant.utils;

import java.io.*;
import java.util.stream.Collectors;

import static io.github.rebelizant.utils.NullUtils.isNull;

/**
 * @author rebelizant
 *         Created on 10.12.15.
 */
public final class IOUtils {

    /**
     * Reads file and returns its content
     *
     * @param filename full qualified name
     * @return content of the file
     */
    public static String fileContent(String filename) {
        return fileContent(new File(filename));
    }

    /**
     * Looks for a resource in the classpath, reads it and returns result
     *
     * @param resourceName name of the resource
     * @return content of the resource
     */
    public static String resourceContent(String resourceName) {
        return toString(IOUtils.class.getClassLoader().getResourceAsStream(resourceName), null);
    }

    /**
     * Reads file and returns its content
     *
     * @param file file to read
     * @return content of the file
     */
    public static String fileContent(File file) {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            return br.lines().collect(Collectors.joining(System.lineSeparator()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            closeQuietly(br);
        }
    }

    public static String toString(InputStream inputStream, String encoding) {
        StringWriter writer = new StringWriter();
        InputStreamReader reader = null;
        try {
            if (isNull(encoding)) {
                reader = new InputStreamReader(inputStream);
            } else {
                reader = new InputStreamReader(inputStream, encoding);
            }
            char[] buffer = new char[2048];
            int charsCount;
            while (-1 != (charsCount = reader.read(buffer))) {
                writer.write(buffer, 0, charsCount);
            }
            return writer.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            writer.flush();
            closeQuietly(writer);
            closeQuietly(reader);
        }
    }

    /**
     * Recreates file:
     * <ol>
     *     <li>Delete file</li>
     *     <li>Create new file</li>
     * </ol>
     *
     * @param path path to file
     * @param filename name of the file
     * @return instance of the created File
     */
    public static File recreateFile(String path, String filename) {
        //noinspection ResultOfMethodCallIgnored
        new File(path).mkdirs();
        File file = new File(path + filename);
        //noinspection ResultOfMethodCallIgnored
        file.delete();
        try {
            //noinspection ResultOfMethodCallIgnored
            file.createNewFile();
            return file;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Rewrites file content with the specified one
     *
     * @param file file to rewrite
     * @param data new file's content
     */
    public static void writeFile(File file, String data) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(file));
            writer.write(data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            closeQuietly(writer);
        }
    }

    /**
     * Closes {@link Closeable} instance if it is not null
     *
     * @param closeable objects that implements {@link Closeable}
     */
    public static void closeQuietly(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
