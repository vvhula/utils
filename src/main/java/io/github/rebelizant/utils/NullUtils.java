package io.github.rebelizant.utils;

import java.util.Arrays;
import java.util.List;

/**
 * Utility class for {@code null} value
 *
 * @author rebelizant
 *         Created on 05.12.15.
 */
public final class NullUtils {

    /**
     * Returns {@code true} if the specified value is {@code null}, {@code false} - otherwise
     *
     * @param obj target value
     * @param <T> type of the value
     * @return {@code true} if value is {@code null}, {@code false} - otherwise
     */
    public static <T> boolean isNull(T obj) {
        return obj == null;
    }

    /**
     * Returns {@code true} if the specified value is NOT {@code null}, {@code false} - otherwise
     *
     * @param obj target value
     * @param <T> type of the value
     * @return {@code true} if value is NOT {@code null}, {@code false} - otherwise
     */
    public static <T> boolean isNotNull(T obj) {
        return !isNull(obj);
    }

    /**
     * Returns {@code true} if any value is {@code null}, {@code false} - otherwise(no value that is {@code null})
     *
     * @param objects target values
     * @param <T> type of the values
     * @return {@code true} if any value is {@code null}, {@code false} - otherwise
     */
    @SafeVarargs
    public static <T> boolean anyNull(T... objects) {
        return isNull(objects) || anyNull(false, Arrays.asList(objects));
    }

    private static <T> boolean anyNull(boolean result, List<T> objects) {
        if (isNull(objects) || objects.isEmpty()) return result;
        return anyNull(result || isNull(objects.get(0)), objects.subList(1, objects.size()));
    }

    /**
     * Returns {@code true} if no value is {@code null}, {@code false} - otherwise
     *
     * @param objects target values
     * @param <T> type of the values
     * @return {@code true} if no value is {@code null}, {@code false} - otherwise
     */
    @SafeVarargs
    public static <T> boolean noNull(T... objects) {
        return isNotNull(objects) && noNull(true, Arrays.asList(objects));
    }

    private static <T> boolean noNull(boolean result, List<T> objects) {
        if (isNull(objects) || objects.size() < 1) return result;
        return noNull(result && isNotNull(objects.get(0)), objects.subList(1, objects.size()));
    }

    /**
     * Throws {@link IllegalArgumentException} if target object is {@code null}
     *
     * @param target object to check
     * @param message error message format
     * @param msgArgs arguments for message
     * @param <T> type of the object
     */
    public static <T> void throwIllegalArgumentExceptionIfNull(T target, String message, String... msgArgs) {
        if (isNull(target)) {
            throw new IllegalArgumentException(String.format(message, msgArgs));
        }
    }

    /**
     * Throws {@link IllegalArgumentException} if target string is {@code null} or empty
     *
     * @param target string to check
     * @param message error message format
     * @param msgArgs arguments for the message
     */
    public static void throwIllegalArgumentExceptionIfEmpty(String target, String message, String... msgArgs) {
        if (isNull(target) || target.length() < 1) {
            throw new IllegalArgumentException(String.format(message, msgArgs));
        }
    }
}
