package io.github.rebelizant.utils;

import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.function.Supplier;

/**
 * @author rebelizant
 *         Created on 05.12.15.
 */
public class If<T> {

    protected Supplier<Boolean> condition;

    protected Callable<T> callable;

    protected Runnable runnable;

    protected If(Supplier<Boolean> condition) {
        this.condition = condition;
    }

    protected If(Boolean condition) {
        this.condition = () -> condition;
    }

    public static <T> RunIf<T> condition(Boolean condition) {
        return new RunIf<>(condition);
    }

    public static <T> RunIf<T> run(Runnable runnable) {
        return new RunIf<>(runnable);
    }

    public static <T> GetIf<T> get(Boolean condition) {
        return new GetIf<>(condition);
    }

    public static <T> CallStoreIf<T> call(Boolean condition) {
        return new CallStoreIf<>(condition);
    }

    public static <T> RunIf<T> condition(Supplier<Boolean> condition) {
        return new RunIf<>(condition);
    }

    public static <T> GetIf<T> get(Supplier<Boolean> condition) {
        return new GetIf<>(condition);
    }

    public static <T> CallStoreIf<T> call(Supplier<Boolean> condition) {
        return new CallStoreIf<>(condition);
    }

    public static class RunIf<T> extends If<T> {

        private RunIf(Supplier<Boolean> condition) {
            super(condition);
        }

        private RunIf(Boolean condition) {
            super(condition);
        }

        public RunIf(Runnable runnable) {
            super(false);
            this.runnable = runnable;
        }

        public void ifTrue(Boolean condition) {
            if (condition) {
                runnable.run();
            }
        }

        public RunIf<T> ifTrue(Runnable runnable) {
            if (condition.get()) {
                runnable.run();
            }
            return this;
        }

        public RunIf<T> ifFalse(Runnable runnable) {
            if (!condition.get()) {
                runnable.run();
            }
            return this;
        }
    }

    public static class GetIf<T> extends If<T> {

        private GetIf(Supplier<Boolean> condition) {
            super(condition);
        }

        private GetIf(Boolean condition) {
            super(condition);
        }

        public <V> Optional<V> getIfTrue(Callable<V> callable) {
            if (condition.get()) {
                return execute(callable);
            } else {
                return Optional.empty();
            }
        }

        public <V> Optional<V> getIfFalse(Callable<V> callable) {
            if (!condition.get()) {
                return execute(callable);
            } else {
                return Optional.empty();
            }
        }

        public <V> Optional<V> getIfTrue(V value) {
            if (condition.get()) {
                return Optional.ofNullable(value);
            } else {
                return Optional.empty();
            }
        }

        public <V> Optional<V> getIfFalse(V value) {
            if (!condition.get()) {
                return Optional.ofNullable(value);
            } else {
                return Optional.empty();
            }
        }

        private <V> Optional<V> execute(Callable<V> callable) {
            try {
                return Optional.ofNullable(callable.call());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static class CallStoreIf<T> extends If<T> {

        private Optional<T> result = Optional.empty();

        private CallStoreIf(Supplier<Boolean> condition) {
            super(condition);
        }

        private CallStoreIf(Boolean condition) {
            super(condition);
        }

        public  CallStoreIf<T> callIfTrue(Callable<T> callable) {
            if (condition.get()) {
                callAndSaveResult(callable);
            }
            return this;
        }

        public CallStoreIf<T> saveIfTrue(T value) {
            if (condition.get()) {
                result = Optional.ofNullable(value);
            }
            return this;
        }

        public CallStoreIf<T> callIfFalse(Callable<T> callable) {
            if (!condition.get()) {
                callAndSaveResult(callable);
            }
            return this;
        }

        public CallStoreIf<T> saveIfFalse(T value) {
            if (!condition.get()) {
                result = Optional.ofNullable(value);
            }
            return this;
        }

        public Optional<T> get() {
            return result;
        }

        private void callAndSaveResult(Callable<T> callable) {
            try {
                result = Optional.ofNullable(callable.call());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

    }

}
