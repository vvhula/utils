package io.github.rebelizant.utils;

import java.util.Random;

/**
 * @author rebelizant
 *         Created on 21.02.16.
 */
public class RandomUtils {

    static Random random = new Random();

    public static String randomNumeric(int length) {
        StringBuilder result = new StringBuilder();
        Iter.times(length).execute(() -> result.append(random.nextInt(10)));
        return result.toString();
    }

}
