package io.github.rebelizant.builder;

/**
 * Abstract implementation of the <code>Builder</code> interface
 *
 * @param <T> type of the object which should be instantiated.
 *
 * @author rebelizant
 *         Created on 05.12.15.
 *
 * @see Builder
 */
public abstract class AbstractBuilder<T> implements Builder<T> {

    /**
     * Constructs new object of the type {@code T}
     *
     * @throws IllegalStateException if {@code this.isValid()} returns {@code false}
     *
     * @return new object constructed from the builder's parameters
     */
    @Override
    public T build() {
        if (isValid()) {
            return buildValid();
        } else {
            throw new IllegalStateException(String.format("Some fields are not valid. Caused by:\n%s", errorMessage()));
        }
    }

    /**
     * Returns new object of the type T
     *
     * @return new object created with builder's values
     */
    protected abstract T buildValid();

    @Override
    public boolean isValid() {
        return true;
    }

    /**
     * Returns error message that indicates illegal state of the builder
     *
     * @return error message
     */
    protected String errorMessage() {
        return "Error";
    }

}
