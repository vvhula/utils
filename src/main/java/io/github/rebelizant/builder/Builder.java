package io.github.rebelizant.builder;

/**
 * The interface for builder hierarchy. A builder represents
 * a class which used to construct some objects.
 *
 * @param <T> type of the object which should be instantiated by the builder
 *
 * @author rebelizant
 *         Created on 05.12.15.
 */
public interface Builder<T> {

    /**
     * Constructs object using parameters from the builder
     *
     * @return new object of the type T
     */
    T build();

    /**
     * Returns <code>true</code> if builder is constructed properly and thus can create object.
     * Returns <code>false</code> otherwise.
     * @return {@code true} if object can be constructed, {@code false} - otherwise
     */
    boolean isValid();

}
