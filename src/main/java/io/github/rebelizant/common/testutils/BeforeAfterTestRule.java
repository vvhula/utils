package io.github.rebelizant.common.testutils;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 * @author rebelizant
 *         Created on 08.01.16.
 */
public abstract class BeforeAfterTestRule implements TestRule {

    @Override
    public Statement apply(Statement statement, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                try {
                    before();
                    statement.evaluate();
                } finally {
                    after();
                }
            }
        };
    }

    protected abstract void before() throws Exception;

    protected abstract void after() throws Exception;


}
