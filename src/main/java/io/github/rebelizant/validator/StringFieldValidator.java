package io.github.rebelizant.validator;

/**
 * @author rebelizant
 *         Created on 13.12.15.
 */
public interface StringFieldValidator {

    boolean validate(String value, String content);

}
