package io.github.rebelizant.validator;

/**
 * Represents value for the fields which annotated
 *
 * @author rebelizant
 *         Created on 11.12.15.
 * @param <V> type of the value to validate
 */
public interface FieldValidator<V> {

    /**
     * Returns {@code true} if the specified value conforms the requirements of the value
     * {@code false} - otherwise
     *
     * @param value value to validate
     * @return {@code true} if value is valid, {@code false} - otherwise
     */
    boolean valid(V value);

    /**
     * Returns description of the validation that performed by this value
     *
     * @return description of the value
     */
    String description();


}
