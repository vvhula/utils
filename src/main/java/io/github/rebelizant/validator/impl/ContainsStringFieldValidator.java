package io.github.rebelizant.validator.impl;

import io.github.rebelizant.utils.NullUtils;
import io.github.rebelizant.validator.AbstractFieldValidator;

/**
 * @author rebelizant
 *         Created on 16.12.15.
 */
public class ContainsStringFieldValidator extends AbstractFieldValidator<String, String> {

    public ContainsStringFieldValidator(String additionalValue) {
        super(additionalValue);
    }

    @Override
    public boolean valid(String value) {
        return NullUtils.isNotNull(value) && value.contains(additionalValue);
    }

    @Override
    public String description() {
        return String.format("Value does not contain specified string: %s", additionalValue);
    }
}
