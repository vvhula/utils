package io.github.rebelizant.validator.impl;

import io.github.rebelizant.validator.AbstractFieldValidator;

/**
 * Validator that always return true
 *
 * @author rebelizant
 *         Created on 11.12.15.
 */
public class SimpleValidator<V> extends AbstractFieldValidator<V, Object> {

    @Override
    public boolean valid(V value) {
        return true;
    }

    @Override
    public String description() {
        return "Validator that always returns true";
    }

}
