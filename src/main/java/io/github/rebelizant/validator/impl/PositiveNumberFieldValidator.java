package io.github.rebelizant.validator.impl;

import io.github.rebelizant.validator.AbstractFieldValidator;
import io.github.rebelizant.validator.FieldValidator;

/**
 * Validator for checking whether the specified value greater than {@code 0} or not
 *
 * @author rebelizant
 *         Created on 13.12.15.
 *
 * @see FieldValidator
 */
public class PositiveNumberFieldValidator extends AbstractFieldValidator<Number, Object> {

    @Override
    public boolean valid(Number value) {
        return value.doubleValue() > 0;
    }

    @Override
    public String description() {
        return "Number should be greater than zero";
    }

}
