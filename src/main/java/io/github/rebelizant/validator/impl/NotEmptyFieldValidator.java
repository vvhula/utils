package io.github.rebelizant.validator.impl;

import io.github.rebelizant.utils.NullUtils;
import io.github.rebelizant.validator.AbstractFieldValidator;

import java.util.Collection;
import java.util.Map;

/**
 * Checks whether the specified value is empty or not
 *
 * @author rebelizant
 *         Created on 13.12.15.
 */
public class NotEmptyFieldValidator<V> extends AbstractFieldValidator<V, Object> {

    @Override
    public boolean valid(V value) {
        if (value instanceof CharSequence) {
            return ((CharSequence) value).length() > 0;
        } else if (value instanceof Collection) {
            return ((Collection) value).size() > 0;
        } else if (value instanceof Map) {
            return ((Map) value).size() > 0;
        } else {
            return NullUtils.isNotNull(value);
        }
    }

    @Override
    public String description() {
        return "Value should not be empty";
    }
}
