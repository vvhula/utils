package io.github.rebelizant.validator.impl;

import io.github.rebelizant.utils.NullUtils;
import io.github.rebelizant.validator.AbstractFieldValidator;
import io.github.rebelizant.validator.FieldValidator;

/**
 * Validator for checking whether the specified value is {@code null} or not
 *
 * @author rebelizant
 *         Created on 12.12.15.
 * @see FieldValidator
 */
public class NotNullValidator<V> extends AbstractFieldValidator<V, Object> {

    @Override
    public boolean valid(V value) {
        return NullUtils.isNotNull(value);
    }

    @Override
    public String description() {
        return "The value of the field should not be null.";
    }
}
