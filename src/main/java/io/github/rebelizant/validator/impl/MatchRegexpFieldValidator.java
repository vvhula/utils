package io.github.rebelizant.validator.impl;

import io.github.rebelizant.validator.AbstractFieldValidator;

/**
 * @author rebelizant
 *         Created on 16.12.15.
 */
public class MatchRegexpFieldValidator extends AbstractFieldValidator<String, String> {

    public MatchRegexpFieldValidator(String additionalValue) {
        super(additionalValue);
    }

    @Override
    public boolean valid(String value) {
        return value.matches(additionalValue);
    }

    @Override
    public String description() {
        return String.format("Value does not match regular expression: %s", additionalValue);
    }
}
