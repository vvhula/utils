package io.github.rebelizant.validator;

/**
 * @author rebelizant
 *         Created on 16.12.15.
 */
public abstract class AbstractFieldValidator<V, T> implements FieldValidator<V> {

    protected T additionalValue;

    public AbstractFieldValidator(){}

    public AbstractFieldValidator(T additionalValue) {
        this.additionalValue = additionalValue;
    }

}
