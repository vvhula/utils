package io.github.rebelizant.validator;

import io.github.rebelizant.builder.AbstractBuilder;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is used to specify value for the field in the class
 *
 * @author rebelizant
 *         Created on 11.12.15.
 *
 * @see AbstractBuilder
 * @see FieldValidator
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
public @interface Validator {

    /**
     * Implementation of the {@link FieldValidator}
     *
     * @see FieldValidator
     */
    Class<? extends FieldValidator> value();

}
