package io.github.rebelizant.validator.exception;

import io.github.rebelizant.validator.FieldValidator;

/**
 * Describes the failure of the validation
 *
 * @author rebelizant
 *         Created on 13.12.15.
 */
public class ValidatorException extends RuntimeException {

    public ValidatorException(Class<? extends FieldValidator> validator, Class<?> valueType) {
        super(String.format("%s: Validation cannot be applied to %s"
                , validator.getSimpleName(), valueType.getSimpleName()));
    }

}
