package io.github.rebelizant.validator;

import io.github.rebelizant.utils.Pair;
import io.github.rebelizant.utils.ReflectionUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import static io.github.rebelizant.utils.ReflectionUtils.*;

/**
 * @author rebelizant
 *         Created on 16.12.15.
 */
public interface FieldValidationCapable {

    default List<Pair<String, FieldValidator>> fieldErrors() {
        List<Pair<String, FieldValidator>> errors= new ArrayList<>();
        for (Field field : fieldsAnnotatedWith(this, Validator.class)) {
            Validator annotation = field.getAnnotation(Validator.class);
            Class<? extends FieldValidator> validator = annotation.value();
            @SuppressWarnings("unchecked")
            FieldValidator<Object> fieldValidator = ReflectionUtils.instance(validator);
            if (!fieldValidator.valid(getDeclaredFieldValue(this, field))) {
                Pair<String, FieldValidator> of = Pair.of(field.getName(), fieldValidator);
                errors.add(of);
            }
        }
        for (Field field : fieldsMarkedWith(this, Validator.class)) {
            for (Annotation a : field.getAnnotations()) {
                if (isAnnotatedWith(a.annotationType(), Validator.class)) {
                    for (Validator v : a.annotationType().getAnnotationsByType(Validator.class)) {
                        Class<? extends FieldValidator> validator = v.value();
                        FieldValidator fieldValidator = ReflectionUtils.instance(validator);
                        @SuppressWarnings("unchecked")
                        boolean isValid = fieldValidator.valid(getDeclaredFieldValue(this, field));
                        if (!isValid) {
                            Pair<String, FieldValidator> of = Pair.of(field.getName(), fieldValidator);
                            errors.add(of);
                        }
                    }

                }
            }
        }
        return errors;
    }

}
