package io.github.rebelizant.validator.annotations;

import io.github.rebelizant.validator.Validator;
import io.github.rebelizant.validator.impl.ContainsStringFieldValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author rebelizant
 *         Created on 16.12.15.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Validator(value = ContainsStringFieldValidator.class)
public @interface Contains {

    String value();

}
