package io.github.rebelizant.validator.annotations;

import io.github.rebelizant.validator.Validator;
import io.github.rebelizant.validator.impl.NotNullValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author rebelizant
 *         Created on 13.12.15.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Validator(value = NotNullValidator.class)
public @interface NotNull {
}
