package io.github.rebelizant.builder;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author rebelizant
 *         Created on 09.12.15.
 */
public class TestBuilder {

    public static final int ID = 1;
    public static final String NAME = "John";
    public static final String EMAIL = "john@example.com";

    public static final String NAME_FIELD = "name";
    public static final String ID_FIELD = "id";

    public static final String FIELD_COMMA_FIELD = "\\w+[%s]\\w+";

    @Test
    public void testBuilderBuildValues() {
        TestClass john = TestClass.builder().id(ID).name(NAME).build();
        assertEquals(ID, john.getId());
        assertEquals(NAME, john.getName());
        assertNull(john.getEmail());
    }

    @Test(expected = IllegalStateException.class)
    public void testFailure() {
        TestClass.builder().build();
    }

    @Test(expected = IllegalStateException.class)
    public void testBuilderNameRequired() {
        TestClass.builder().id(ID).email(EMAIL).build();
    }

    @Test(expected = IllegalStateException.class)
    public void testBuilderIdRequired() {
        TestClass.builder().name(NAME).build();
    }

    @Test
    public void testErrorMessage() {
        String expectedErrorMessage = "Some fields are not valid. Caused by:\n" +
                "id, name should not be null. id should be greater than zero";
        try {
            TestClass.builder().build();
        } catch (IllegalStateException e) {
            assertEquals(expectedErrorMessage, e.getMessage());
        }
    }

    @Test
    public void testBuilderIsValid() {
        TestClass.Builder builder = TestClass.builder();
        builder.id(ID);
        assertFalse(builder.isValid());
        builder.name(NAME);
        assertTrue(builder.isValid());
        builder.email(EMAIL);
        TestClass john = builder.build();
        assertEquals(ID, john.getId());
        assertEquals(NAME, john.getName());
        assertEquals(EMAIL, john.getEmail());
    }

}
