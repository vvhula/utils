package io.github.rebelizant.builder;

import io.github.rebelizant.utils.NullUtils;

/**
 * @author rebelizant
 *         Created on 06.12.15.
 */
public class TestClass {

    private int id;
    private String name;
    private String email;

    protected TestClass(int id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends AbstractBuilder<TestClass> {

        private int id;

        private String name;

        private String email;

        private String employeeNumber = "EMP_123";

        private String startsWithNumber = "1sds";

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        @Override
        protected TestClass buildValid() {
            return new TestClass(id, name, email);
        }

        @Override
        protected String errorMessage() {
            return "id, name should not be null. id should be greater than zero";
        }

        @Override
        public boolean isValid() {
            return NullUtils.noNull(id, name) && id > 0;
        }
    }

}
