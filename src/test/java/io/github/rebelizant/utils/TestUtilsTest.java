package io.github.rebelizant.utils;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * @author rebelizant
 *         Created on 10.01.16.
 */
public class TestUtilsTest {

    private static final String ERROR_MESSAGE = "error message";

    @SuppressWarnings("ThrowableInstanceNeverThrown")
    private static final IllegalAccessException CAUSE = new IllegalAccessException(ERROR_MESSAGE);

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testThrowCause() throws Throwable {
        expectedException.expect(CAUSE.getClass());
        expectedException.expectMessage(ERROR_MESSAGE);
        TestUtils.throwCause(() -> {
            throw new IllegalArgumentException(CAUSE);
        });
    }
}