package io.github.rebelizant.utils;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static io.github.rebelizant.utils.IOUtils.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 * @author rebelizant
 *         Created on 10.01.16.
 */
public class IOUtilsTest {

    private static final String TEST_PATH = "./src/test/resources/";

    private static final String TEST_FILE_CONTENT = "Hello, World!\nNew Line.";
    private static final String EMPTY_STRING = "";
    private static final String ERROR_MESSAGE = "error message";

    @ClassRule
    public static SetupFile fileWithContent = new SetupFile("test.txt", TEST_PATH, TEST_FILE_CONTENT);

    @ClassRule
    public static SetupFile recreateFile = new SetupFile("recreateFile", TEST_PATH, TEST_FILE_CONTENT);

    @ClassRule
    public static SetupFile emptyFile = new SetupFile("emptyFile", TEST_PATH, EMPTY_STRING);

    @Mock
    private InputStream inputStream;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setupTest() throws IOException {
        MockitoAnnotations.initMocks(this);
        Mockito.doThrow(new IOException(ERROR_MESSAGE)).when(inputStream).close();
    }

    @Test
    public void testFileContent() throws Exception {
        assertThat(fileContent(fileWithContent.getFQN()), equalTo(fileWithContent.getFileContent()));
        assertThat(fileContent(new File(fileWithContent.getFQN())), equalTo(fileWithContent.getFileContent()));
    }

    @Test
    public void testRecreateFile() throws Exception {
        assertThat(fileContent(recreateFile.getFQN()), equalTo(recreateFile.getFileContent()));
        recreateFile(recreateFile.getPath(), recreateFile.getFilename());
        assertThat(fileContent(recreateFile.getFQN()), equalTo(EMPTY_STRING));
    }

    @Test
    public void testWriteFile() throws Exception {
        assertThat(fileContent(emptyFile.getFQN()), equalTo(EMPTY_STRING));
        writeFile(new File(emptyFile.getFQN()), TEST_FILE_CONTENT);
        assertThat(fileContent(emptyFile.getFQN()), equalTo(TEST_FILE_CONTENT));
        writeFile(new File(emptyFile.getFQN()), TEST_FILE_CONTENT);
        assertThat(fileContent(emptyFile.getFQN()), equalTo(TEST_FILE_CONTENT));
    }

    @Test
    public void testCloseQuietly() throws Exception {
        IOUtils.closeQuietly(new ByteArrayInputStream(new byte[]{1, 2, 3}));
    }

    @Test
    public void testCloseQuietlyError() {
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage(ERROR_MESSAGE);
        expectedException.expectCause(isA(IOException.class));
        IOUtils.closeQuietly(inputStream);
    }

    @Test
    public void testToString() throws Exception {
        assertThat(fileWithContent.getAsInputStream(), notNullValue());
        assertThat(IOUtils.toString(fileWithContent.getAsInputStream(), null), equalTo(TEST_FILE_CONTENT));
        assertThat(IOUtils.toString(fileWithContent.getAsInputStream(), "utf-8"), equalTo(TEST_FILE_CONTENT));
    }

    @Test
    public void testResourceContent() throws Exception {
        String content = resourceContent("data/test_data.txt");
        assertThat(content, notNullValue());
        assertThat(content, equalTo("Hello World!"));
    }
}