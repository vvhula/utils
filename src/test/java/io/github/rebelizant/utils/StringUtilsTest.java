package io.github.rebelizant.utils;

import org.junit.Test;

import static io.github.rebelizant.utils.StringUtils.*;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author rebelizant
 *         Created on 10.01.16.
 */
public class StringUtilsTest {

    private static final String NULL = null;
    private static final String EMPTY_STRING = "";
    private static final String NON_EMPTY_STRING = "Hello, World!";
    private static final String ONE_CHAR_STRING = "A";

    @Test
    public void testIsEmpty() throws Exception {
        assertThat(isEmpty(NULL), is(TRUE));
        assertThat(isEmpty(EMPTY_STRING), is(TRUE));
        assertThat(isEmpty(NON_EMPTY_STRING), is(FALSE));
        assertThat(isEmpty(ONE_CHAR_STRING), is(FALSE));
    }

    @Test
    public void testIsNotEmpty() throws Exception {
        assertThat(isNotEmpty(NULL), is(FALSE));
        assertThat(isNotEmpty(EMPTY_STRING), is(FALSE));
        assertThat(isNotEmpty(NON_EMPTY_STRING), is(TRUE));
        assertThat(isNotEmpty(ONE_CHAR_STRING), is(TRUE));
    }

    @Test
    public void testNoEmpty() throws Exception {
        assertThat(noEmpty(NON_EMPTY_STRING), is(TRUE));
        assertThat(noEmpty(NON_EMPTY_STRING, NON_EMPTY_STRING), is(TRUE));
        assertThat(noEmpty(NULL), is(FALSE));
        assertThat(noEmpty(NULL, NULL), is(FALSE));
        assertThat(noEmpty(EMPTY_STRING), is(FALSE));
        assertThat(noEmpty(EMPTY_STRING, EMPTY_STRING), is(FALSE));
        assertThat(noEmpty(EMPTY_STRING, NON_EMPTY_STRING, ONE_CHAR_STRING), is(FALSE));
        assertThat(noEmpty(NULL, NON_EMPTY_STRING, ONE_CHAR_STRING), is(FALSE));
        assertThat(noEmpty(NULL, EMPTY_STRING), is(FALSE));
    }

    @Test
    public void testAllEmpty() {
        assertThat(allEmpty(NULL), is(TRUE));
        assertThat(allEmpty(NULL, NULL), is(TRUE));
        assertThat(allEmpty(EMPTY_STRING), is(TRUE));
        assertThat(allEmpty(EMPTY_STRING, EMPTY_STRING), is(TRUE));
        assertThat(allEmpty(NULL, EMPTY_STRING), is(TRUE));
        assertThat(allEmpty(NON_EMPTY_STRING), is(FALSE));
        assertThat(allEmpty(NON_EMPTY_STRING, NULL), is(FALSE));
        assertThat(allEmpty(NON_EMPTY_STRING, NULL, EMPTY_STRING), is(FALSE));
    }
}