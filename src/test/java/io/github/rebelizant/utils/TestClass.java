package io.github.rebelizant.utils;

/**
 * @author rebelizant
 *         Created on 06.12.15.
 */
@SomeAnnotation
public class TestClass {

    public static final Integer DEFAULT_ID_VALUE = 1;

    @SomeAnnotation
    private String name;

    @SuppressWarnings("FieldCanBeLocal")
    private final Integer id = DEFAULT_ID_VALUE;

    public TestClass(){}

    public String getName() {
        return name;
    }

    public Integer getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
