package io.github.rebelizant.utils;

/**
 * @author rebelizant
 *         Created on 09.12.15.
 */
public class Counter {
    public int x = 0;
    public void incX() {
        x++;
    }
    public void decX() {
        x--;
    }
}
