package io.github.rebelizant.utils;

import org.junit.Test;

import java.io.FileNotFoundException;
import java.util.Optional;
import java.util.function.Supplier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * @author rebelizant
 *         Created on 06.12.15.
 */
public class IfTest {

    public static final Integer ZERO = 0;
    public static final Integer ONE = 1;

    @Test
    public void testGetIfSupplierCondition() {
        Supplier<Boolean> trueSupplier = () -> ONE > ZERO;
        Optional<Integer> result = If.get(trueSupplier).getIfTrue(ONE);
        assertEquals(ONE, result.get());

        result = If.get(trueSupplier).getIfFalse(ONE);
        assertFalse(result.isPresent());

        result = If.get(trueSupplier).getIfTrue(() -> ONE);
        assertEquals(ONE, result.get());

        result = If.get(trueSupplier).getIfFalse(() -> ONE);
        assertFalse(result.isPresent());
    }

    @Test
    public void testGetIfBoolCondition() {
        boolean condition = ONE > ZERO;

        Optional<Integer> result = If.get(condition).getIfTrue(ONE);
        assertEquals(ONE, result.get());

        result = If.get(condition).getIfFalse(ONE);
        assertFalse(result.isPresent());

        result = If.get(condition).getIfTrue(() -> ONE);
        assertEquals(ONE, result.get());

        result = If.get(condition).getIfFalse(() -> ONE);
        assertFalse(result.isPresent());
    }

    @Test
    public void testCallIfSupplierConditionTrue() {
        //---true
        Supplier<Boolean> trueSupplier = () -> ONE > ZERO;
        Optional<Integer> result = If.<Integer>call(trueSupplier)
                .callIfTrue(() -> ONE)
                .callIfFalse(() -> ZERO)
                .get();
        assertEquals(ONE, result.get());

        result = If.<Integer>call(trueSupplier)
                .saveIfTrue(ONE)
                .callIfFalse(() -> ZERO)
                .get();
        assertEquals(ONE, result.get());

        result = If.<Integer>call(trueSupplier)
                .callIfTrue(() -> ONE)
                .saveIfFalse(ZERO)
                .get();
        assertEquals(ONE, result.get());

        result = If.<Integer>call(trueSupplier)
                .saveIfTrue(ONE)
                .saveIfFalse(ZERO)
                .get();
        assertEquals(ONE, result.get());

        //---true
        result = If.<Integer>call(trueSupplier)
                .callIfTrue(() -> ONE)
                .get();
        assertEquals(ONE, result.get());

        result = If.<Integer>call(trueSupplier)
                .saveIfTrue(ONE)
                .get();
        assertEquals(ONE, result.get());

        //---true
        result = If.<Integer>call(trueSupplier)
                .callIfFalse(() -> ZERO)
                .get();
        assertFalse(result.isPresent());

        result = If.<Integer>call(trueSupplier)
                .saveIfFalse(ZERO)
                .get();
        assertFalse(result.isPresent());
    }

    @Test
    public void testCallIfBooleanConditionTrue() {
        //---true
        Optional<Integer> result = If.<Integer>call(true)
                .callIfTrue(() -> ONE)
                .callIfFalse(() -> ZERO)
                .get();
        assertEquals(ONE, result.get());

        result = If.<Integer>call(true)
                .saveIfTrue(ONE)
                .callIfFalse(() -> ZERO)
                .get();
        assertEquals(ONE, result.get());

        result = If.<Integer>call(true)
                .callIfTrue(() -> ONE)
                .saveIfFalse(ZERO)
                .get();
        assertEquals(ONE, result.get());

        result = If.<Integer>call(true)
                .saveIfTrue(ONE)
                .saveIfFalse(ZERO)
                .get();
        assertEquals(ONE, result.get());

        //---true
        result = If.<Integer>call(true)
                .callIfTrue(() -> ONE)
                .get();
        assertEquals(ONE, result.get());

        result = If.<Integer>call(true)
                .saveIfTrue(ONE)
                .get();
        assertEquals(ONE, result.get());

        //---true
        result = If.<Integer>call(true)
                .callIfFalse(() -> ZERO)
                .get();
        assertFalse(result.isPresent());

        result = If.<Integer>call(true)
                .saveIfFalse(ZERO)
                .get();
        assertFalse(result.isPresent());
    }

    @Test
    public void testCallIfSupplierConditionFalse() {
        //---false
        Supplier<Boolean> falseSupplier = () -> ONE < ZERO;
        Optional<Integer> result = If.<Integer>call(falseSupplier)
                .callIfTrue(() -> ONE)
                .callIfFalse(() -> ZERO)
                .get();
        assertEquals(ZERO, result.get());

        result = If.<Integer>call(falseSupplier)
                .saveIfTrue(ONE)
                .callIfFalse(() -> ZERO)
                .get();
        assertEquals(ZERO, result.get());

        result = If.<Integer>call(falseSupplier)
                .callIfTrue(() -> ONE)
                .saveIfFalse(ZERO)
                .get();
        assertEquals(ZERO, result.get());

        result = If.<Integer>call(falseSupplier)
                .saveIfTrue(ONE)
                .saveIfFalse(ZERO)
                .get();
        assertEquals(ZERO, result.get());

        //---false
        result = If.<Integer>call(falseSupplier)
                .callIfFalse(() -> ZERO)
                .get();
        assertEquals(ZERO, result.get());

        result = If.<Integer>call(falseSupplier)
                .saveIfFalse(ZERO)
                .get();
        assertEquals(ZERO, result.get());

        //---false
        result = If.<Integer>call(falseSupplier)
                .callIfTrue(() -> ONE)
                .get();
        assertFalse(result.isPresent());

        result = If.<Integer>call(falseSupplier)
                .saveIfTrue(ONE)
                .get();
        assertFalse(result.isPresent());
    }

    @Test
    public void testCallIfBooleanConditionFalse() {
        //---false
        Optional<Integer> result = If.<Integer>call(false)
                .callIfTrue(() -> ONE)
                .callIfFalse(() -> ZERO)
                .get();
        assertEquals(ZERO, result.get());

        result = If.<Integer>call(false)
                .saveIfTrue(ONE)
                .callIfFalse(() -> ZERO)
                .get();
        assertEquals(ZERO, result.get());

        result = If.<Integer>call(false)
                .callIfTrue(() -> ONE)
                .saveIfFalse(ZERO)
                .get();
        assertEquals(ZERO, result.get());

        result = If.<Integer>call(false)
                .saveIfTrue(ONE)
                .saveIfFalse(ZERO)
                .get();
        assertEquals(ZERO, result.get());

        //---false
        result = If.<Integer>call(false)
                .callIfFalse(() -> ZERO)
                .get();
        assertEquals(ZERO, result.get());

        result = If.<Integer>call(false)
                .saveIfFalse(ZERO)
                .get();
        assertEquals(ZERO, result.get());

        //---false
        result = If.<Integer>call(false)
                .callIfTrue(() -> ONE)
                .get();
        assertFalse(result.isPresent());

        result = If.<Integer>call(false)
                .saveIfTrue(ONE)
                .get();
        assertFalse(result.isPresent());
    }

    @Test
    public void testRunSupplierConditionTrue() {
        Counter a = new Counter();
        a.x = 0;
        Supplier<Boolean> trueSupplier = () -> ONE > ZERO;
        If.condition(trueSupplier).ifTrue(a::incX).ifFalse(a::decX);
        assertEquals(1, a.x);
        If.condition(trueSupplier).ifTrue(a::incX);
        assertEquals(2, a.x);
        If.condition(trueSupplier).ifFalse(a::incX);
        assertEquals(2, a.x);
    }

    @Test
    public void testRunBoolConditionTrue() {
        Counter a = new Counter();
        a.x = 0;
        If.condition(true).ifTrue(a::incX).ifFalse(a::decX);
        assertEquals(1, a.x);
        If.condition(true).ifTrue(a::incX);
        assertEquals(2, a.x);
        If.condition(true).ifFalse(a::incX);
        assertEquals(2, a.x);
    }

    @Test
    public void testRunSupplierConditionFalse() {
        Counter a = new Counter();
        a.x = 0;
        Supplier<Boolean> falseSupplier = () -> ONE < ZERO;
        If.condition(falseSupplier).ifTrue(a::incX).ifFalse(a::decX);
        assertEquals(-1, a.x);
        If.condition(falseSupplier).ifFalse(a::decX);
        assertEquals(-2, a.x);
        If.condition(falseSupplier).ifTrue(a::incX);
        assertEquals(-2, a.x);
    }

    @Test
    public void testRunBoolConditionFalse() {
        Counter a = new Counter();
        a.x = 0;
        If.condition(false).ifTrue(a::incX).ifFalse(a::decX);
        assertEquals(-1, a.x);
        If.condition(false).ifFalse(a::decX);
        assertEquals(-2, a.x);
        If.condition(false).ifTrue(a::incX);
        assertEquals(-2, a.x);
    }

    @Test(expected = FileNotFoundException.class)
    public void testCallIfException() throws Throwable {
        TestUtils.throwCause(() -> If.call(() -> true).callIfTrue(() -> {
            throw new FileNotFoundException();
        }));
    }

    @Test(expected = FileNotFoundException.class)
    public void testGetIfException() throws Throwable {
        TestUtils.throwCause(() -> If.get(() -> true).getIfTrue(() -> {
            throw new FileNotFoundException();
        }));
    }


}
