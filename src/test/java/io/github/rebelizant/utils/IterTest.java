package io.github.rebelizant.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author rebelizant
 *         Created on 05.12.15.
 */
public class IterTest {

    @Test
    public void testTimesIter() {
        int times = 5;
        Counter c = new Counter();
        Iter.times(times).execute(c::incX);
        Assert.assertEquals(times, c.x);
        Iter.times(1).execute(c::decX);
        Assert.assertEquals(4, c.x);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTimesIterException() {
        Iter.times(-1).execute(() ->{});
    }

    @Test
    public void testTimesIterSafe() {
        int times = 5;
        Counter c = new Counter();
        Iter.timesSafe(times).execute(c::incX);
        Assert.assertEquals(times, c.x);
        Iter.timesSafe(-2).execute(c::incX);
        Assert.assertEquals(times, c.x);
    }

    @Test
    public void testWhileIter() {
        int loopsCount = 4;
        Counter dec = new Counter();
        dec.x = loopsCount;
        Counter inc = new Counter();
        Iter.whileTrue(() -> dec.x > 0).execute(() -> {
            dec.decX();
            inc.incX();
        });
        Assert.assertEquals(loopsCount, inc.x);
    }

    @Test(expected = IllegalStateException.class)
    public void testWhileIterException() {
        Iter.whileTrue(() -> true).execute(() -> {
            throw new IllegalStateException();
        });
    }

    @Test
    public void testWhileIterFalse() {
        Counter c = new Counter();
        int one = 1;
        c.x = one;
        Iter.whileTrue(() -> c.x > one).execute(c::decX);
        Assert.assertEquals(one, c.x);
    }

}
