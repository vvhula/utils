package io.github.rebelizant.utils;


import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author rebelizant
 *         Created on 05.12.15.
 */
public class NullUtilsTest {

    private static final String ERROR_MESSAGE_FORMAT = "error message: %s";
    private static final String[] ERROR_MSG_ARGS = {"err1"};
    private static final String ERROR_MESSAGE = String.format(ERROR_MESSAGE_FORMAT, ERROR_MSG_ARGS);
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testIsNull() {
        assertTrue(NullUtils.isNull(null));
        assertFalse(NullUtils.isNull(""));
        assertFalse(NullUtils.isNull(new Object()));
    }

    @Test
    public void testIsNotNull() {
        assertFalse(NullUtils.isNotNull(null));
        assertTrue(NullUtils.isNotNull(""));
        assertTrue(NullUtils.isNotNull(new Object()));
    }

    @SuppressWarnings("NullArgumentToVariableArgMethod")
    @Test
    public void testAnyNull() {
        assertTrue(NullUtils.anyNull(1, 2, "", null));
        assertTrue(NullUtils.anyNull(null));
        assertTrue(NullUtils.anyNull(null, null, new Object()));
        assertFalse(NullUtils.anyNull(1, 2, "", new Object()));
        assertFalse(NullUtils.anyNull());
    }

    @SuppressWarnings("NullArgumentToVariableArgMethod")
    @Test
    public void testNoNull() {
        assertTrue(NullUtils.noNull());
        assertTrue(NullUtils.noNull(1, 2, 3, ""));
        assertFalse(NullUtils.noNull(null));
        assertFalse(NullUtils.noNull(null, null));
        assertFalse(NullUtils.noNull(new Object(), null, null));
    }

    @Test
    public void testThrowIllegalArgumentExceptionIfNull() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(ERROR_MESSAGE);
        NullUtils.throwIllegalArgumentExceptionIfNull(null, ERROR_MESSAGE_FORMAT, ERROR_MSG_ARGS);
    }

    @Test
    public void testThrowIllegalArgumentExceptionIfEmpty() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(ERROR_MESSAGE);
        NullUtils.throwIllegalArgumentExceptionIfEmpty("", ERROR_MESSAGE_FORMAT, ERROR_MSG_ARGS);
    }

    @Test
    public void testThrowIllegalArgumentExceptionIfEmpty2() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(ERROR_MESSAGE);
        NullUtils.throwIllegalArgumentExceptionIfEmpty(null, ERROR_MESSAGE_FORMAT, ERROR_MSG_ARGS);
    }
}
