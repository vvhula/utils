package io.github.rebelizant.utils;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import static io.github.rebelizant.utils.TestUtils.throwCause;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author rebelizant
 *         Created on 06.12.15.
 */
public class ReflectionUtilsTest {

    private static final String FIELD_NAME = "name";
    private static final String FIELD_ID = "id";

    private TestClass target = new TestClass();

    @Before
    public void before() {
        target = new TestClass();
    }

    @Test
    public void testIsAnnotatedWith() {
        Field name = ReflectionUtils.getDeclaredField(TestClass.class, FIELD_NAME);
        assertNotNull(name);
        assertTrue(ReflectionUtils.isAnnotatedWith(name, SomeAnnotation.class));
        assertTrue(ReflectionUtils.isAnnotatedWith(TestClass.class, SomeAnnotation.class));
        assertTrue(ReflectionUtils.isAnnotatedWith(target.getClass(), SomeAnnotation.class));
        assertFalse(ReflectionUtils.isAnnotatedWith(name, Deprecated.class));
    }

    @Test
    public void testSetDeclaredField() {
        String value = "John";
        ReflectionUtils.setDeclaredField(target, FIELD_NAME, value);
        assertEquals(value, target.getName());

        value = "Smith";
        ReflectionUtils.setDeclaredField(target, ReflectionUtils.getDeclaredField(target, FIELD_NAME), value);
        assertEquals(value, target.getName());
    }

    @Test
    public void testSetDeclaredFinalField() {
        Integer value = 2;
        ReflectionUtils.setDeclaredField(target, FIELD_ID, value);
        assertEquals(value.intValue(), target.getId().intValue());

        value = 3;
        ReflectionUtils.setDeclaredField(target, ReflectionUtils.getDeclaredField(target, FIELD_ID), value);
        assertEquals(value.intValue(), target.getId().intValue());
    }

    @Test
    public void testGetDeclaredFieldValue() {
        Integer value = ReflectionUtils.getDeclaredFieldValue(target, FIELD_ID);
        assertEquals(TestClass.DEFAULT_ID_VALUE, value);

        String nameValue = "John";
        target.setName(nameValue);
        String name = ReflectionUtils.getDeclaredFieldValue(target, ReflectionUtils
                .getDeclaredField(target, FIELD_NAME));
        assertEquals(nameValue, name);
    }

    @Test
    public void testValueNull() {
        target.setName(null);
        assertTrue(ReflectionUtils.valueNull(target, FIELD_NAME));
        assertTrue(ReflectionUtils.valueNull(target, ReflectionUtils.getDeclaredField(target, FIELD_NAME)));
        target.setName("Smith");
        assertFalse(ReflectionUtils.valueNull(target, ReflectionUtils.getDeclaredField(target, FIELD_NAME)));
        assertFalse(ReflectionUtils.valueNull(target, FIELD_NAME));
    }

    @Test
    public void testValueNotNull() {
        target.setName(null);
        assertFalse(ReflectionUtils.valueNotNull(target, FIELD_NAME));
        assertFalse(ReflectionUtils.valueNotNull(target, ReflectionUtils.getDeclaredField(target, FIELD_NAME)));
        target.setName("Smith");
        assertTrue(ReflectionUtils.valueNotNull(target, FIELD_NAME));
        assertTrue(ReflectionUtils.valueNotNull(target, ReflectionUtils.getDeclaredField(target, FIELD_NAME)));
    }

    @Test
    public void testNoNullValues() {
        Field nameField = ReflectionUtils.getDeclaredField(target, FIELD_NAME);
        Field idField = ReflectionUtils.getDeclaredField(target, FIELD_ID);
        assertFalse(ReflectionUtils.noNullValues(target, nameField, idField));
        target.setName("Smith");
        assertTrue(ReflectionUtils.noNullValues(target, nameField, idField));
    }

    @Test
    public void testGetDeclaredFieldSuccess() {
        Field name = ReflectionUtils.getDeclaredField(TestClass.class, FIELD_NAME);
        assertNotNull(name);
        Field id = ReflectionUtils.getDeclaredField(TestClass.class, FIELD_ID);
        assertNotNull(id);
    }

    @Test(expected = NoSuchFieldException.class)
    public void testGetDeclaredFieldFailure() throws Throwable {
        throwCause(() -> ReflectionUtils.getDeclaredField(TestClass.class, FIELD_NAME + "2"));
    }

    @Test
    public void testInstance() {
        TestClass instance = ReflectionUtils.instance(TestClass.class);
        assertNotNull(instance);
        class A {
            protected A(){}
        }
    }

    @Test(expected = InstantiationException.class)
    public void testInstanceError() throws Throwable {
        class A {
            private A(){}
        }
        throwCause(() -> ReflectionUtils.instance(A.class));
    }
}
