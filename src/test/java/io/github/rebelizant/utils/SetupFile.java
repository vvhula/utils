package io.github.rebelizant.utils;

import io.github.rebelizant.common.testutils.BeforeAfterTestRule;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * @author rebelizant
 *         Created on 10.01.16.
 */
public class SetupFile extends BeforeAfterTestRule {

    private String filename;

    private String path;

    private String fileContent;

    public SetupFile(String filename, String path, String fileContent) {
        this.filename = filename;
        this.path = path;
        this.fileContent = fileContent;
    }

    @Override
    @SuppressWarnings("all")
    protected void before() throws Exception {
        new File(path).mkdirs();
        File file = new File(path + filename);
        file.delete();
        file.createNewFile();
        IOUtils.writeFile(file, fileContent);
    }

    @Override
    @SuppressWarnings("all")
    protected void after() throws Exception {
        new File(path + filename).delete();
    }

    public String getFilename() {
        return filename;
    }

    public String getPath() {
        return path;
    }

    public String getFileContent() {
        return fileContent;
    }

    public String getFQN() {
        return path + filename;
    }

    public InputStream getAsInputStream() throws FileNotFoundException {
        return new FileInputStream(getFQN());
    }
}
