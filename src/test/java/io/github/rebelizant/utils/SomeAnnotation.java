package io.github.rebelizant.utils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author rebelizant
 *         Created on 06.12.15.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface SomeAnnotation {
}
